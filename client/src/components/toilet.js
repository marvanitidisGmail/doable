import React, {Component} from 'react';
import PropTypes from 'prop-types';
import './toilet.css';

class Toilet extends React.Component {
    render() {
        return (
            <div className='home-container'>
                <h1
                    className={this.props.unavailable ? 'asd' : ''}
                    style={{fontSize: 100 + 'px'}}
                >
                    🚽
                </h1>
                <p>
                    {this.props.unavailable}
                </p>
            </div>
        )
    }
}

Toilet.propTypes = {
    unavailable: PropTypes.string,
};

export { Toilet }