import React, {Component} from 'react';
import './App.css';
import {subscribeToTimer} from "./api";
import {Toilet} from "./components/toilet";

class App extends Component {
    constructor(props) {
        super(props);

        subscribeToTimer((err, timestamp) => this.setState({
            timestamp
        }));
    }

    state = {
        timestamp: 'not set'
    };

    render() {
        return (
            <div className="App">
                <p className="App-intro">
                    This is the timer value: {this.state.timestamp}
                </p>
                <Toilet
                    unavailable = {this.state.timestamp}
                />
            </div>
        );
    }
}

export default App;
