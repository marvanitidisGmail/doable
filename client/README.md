Basert på https://medium.com/dailyjs/combining-react-with-socket-io-for-real-time-goodness-d26168429a34

Må ha installert node og npm
- Npm følger med Node, installer [herfra](https://nodejs.org/en/)
- Kjør `npm i`
- Start server med å kjøre `node server`
- Start klienten med å kjøre `npm start`
